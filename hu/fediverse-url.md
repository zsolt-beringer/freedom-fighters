# Fediverse

## Telepítés

* https://github.com/friendica/friendica/blob/develop/doc/Install.md Friendica telepítés ssh-n
* https://github.com/friendica/friendica/wiki/DreamHost-Friendica-Install-Guide
* https://github.com/friendica/docker Kattintásos Friendica telepítés Dockerbe
* https://github.com/YunoHost-Apps/friendica_ynh Kattintásos Friendica telepítés YunoHost-ra

## Meglévő példányok

Összehasonlítások:

* https://the-federation.info/#protocols Szerver és protokoll statisztikák
* https://podupti.me/ Szerver kereső táblázat
  * javasolt szűrőfeltételek:
    * Signups=yes
    * Pod: könnyű legyen kimondani telefonon keresztül
    * Rendezés minél több: 1m Users, Users, Posts
    * Version: nem túl régi
    * Uptime > 99%
    * Months minél több
    * IPv6=yes
    * Country != US
    * esetleg: Language=EN

Friendica:

* https://fika.grin.hu/search?tag=humor (deklarálta, hogy csak hobbiból futtatja és ne építsünk rá)
* https://libranet.de/community
* https://nerdica.net/community (6 havonta legalább egyszer be kell lépni)

Diaspora:

* https://spora.grin.hu/tags/fun (deklarálta, hogy csak hobbiból futtatja és ne építsünk rá)
* https://sysad.org/tags/fun
* http://wk3.org/tags/fun
* http://diaspora.psyco.fr/tags/fun
* https://pod.hoizi.net/tags/humor
* http://iviv.hu/tags/humor Vicces nevű

Mastodon:

* https://mastodon.grin.hu/public (deklarálta, hogy csak hobbiból futtatja és ne építsünk rá)
* https://fosstodon.org/tags/humor
* https://mamot.fr/tags/humor
* https://mas.to/tags/humor


Pleroma

* https://pleroma.site/tag/humor
* https://pleroma.fr/tag/humor

Hubzilla

* https://hub.netzgemeinde.eu/search?tag=humour
* https://hubzilla.pepecyb.hu/pubstream

## Pingback

Ha átnevezésre vagy áthelyezésre kerül ez a fájl, az összes közvetlen hivatkozást frissíteni kell:

* https://hup.hu/comment/2555753#comment-2555753
