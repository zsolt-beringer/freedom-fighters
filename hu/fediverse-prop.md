név |Friendica|Mastodon|Diaspora|Pleroma|Hubzilla
-|-|-|-|-|-
kezdés|2010|2016|2010|2017|2012-2015
magyarítva|nem|igen|igen|félig|nem
utolsó havi fejlesztők|5|15|5|25|3
utolsó félévi közreműködők|100+|100+|100+|100+|51
kódtároló könyvjelző|894|22300|12600|178|43
szerver példányok|262|2829|163|758|kevés
felhasználók|7k|3M|757k|58k|kevés
szerver erőforrásigény|közepes|közepes|közepes|kicsi|közepes
PaaS|PHP|Ruby on Rails|Ruby on Rails|Elixir|PHP
követők|csoportosítható|zárolható|csoportosítható||jogosultságok
láthatóság szabályozása|csoportok, egyének|publikus, követők vagy egyén|csoportok||csoportok, egyének
kategóriák|igen|nem|nem||igen
követés|emberek, címkék|emberek, max 4 címke|emberek, címkék||emberek
események|igen|nem|nem|nem|igen
protokoll|AP, Diaspora|AP|Diaspora|AP|AP, Diaspora, Zot, OStatus
törölhetőség|||nincs||igen
elévíthetőség|||||igen
külső illeszthetőség|RSS, Blogger, Discourse, LiveJournal, Tumblr, WordPress, email||||
feed kimenet|||atom||
manuális falszűrők|igen||||
üzenethossz|korlátlan?|500|vertikálisan lenyitható pár sor után||vertikálisan lenyitható pár sor után
export|igen|igen|igen||
import|korlátozott|korlátozott|nem||
fiók átirányítás||igen|||
válaszok kedvelése|igen|igen|nem||igen
címke böngészés|publikus||publikus|publikus|publikus
lokális üzenőfal|publikus|publikus|||
web: kép|fix|nézőke|fix||nézőke
web: animált kép|automatikus|automatikussá tehető|automatikus||automatikus
web: PeerTube|lejátszó|lejátszó|bélyegkép||bélyegkép
web: emoji választó|statikus, karakteres|kereshető, színes képek|nincs||szöveges autocomplete, 11 statikus
web: PixelFed|bélyegkép, leírás|nem|bélyegkép, leírás||kép, leírás
web: Mobilizon|kép, leírás|nem|bélyegkép, leírás||kép, leírás
web kliens||sengi, Pinaforce, Halcyon, Cuckoo+|||
asztali kliens|Choqok, Friendiqa|Choqok, Tootle, WhaleBird, TheDesk, Gakki, Olifant, Kaiteki, sengi, Hyperspace||WhaleBird, Kaiteki, sengi|
mobil kliens|Fedilab, Fedilab Lite, DiCa-MianXian|Husky, Tusky, AndStatus, Twidere, Fedilab, Fedilab Lite, SubwayTooter, DiCa-MianXian|Dandelion, Insporation, DiCa-MianXian|Husky, Fedilab, Fedilab Lite|Nomad
beágyazható megosztás gomb|"""Advanced Sharer"""|igen|"""Advanced Sharer"", Simple Diaspora Sharing"|még nincs|
